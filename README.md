# README #

If you're using the AppManager with Communitake, please exclude the dependency module: 

```
compile("it.ennova.skillo:appmanager:0.1.0") {
        exclude module: "dependency"
    }
```