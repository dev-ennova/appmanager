package it.ennova.appusagestats

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import it.ennova.appmanager.AppManagerFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment = AppManagerFragment()
        fragment.setDescription("Utile descrizione per la feature")

        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()

    }
}