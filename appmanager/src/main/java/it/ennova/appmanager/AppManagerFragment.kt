package it.ennova.appmanager

import android.annotation.TargetApi
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import it.ennova.appmanager.internal.*
import kotlinx.android.synthetic.main.appmanager_fragment_layout.*
import kotlinx.android.synthetic.main.request_permission_layout.*

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class AppManagerFragment : Fragment(), UsageContract.View, PackageListener {

    private val presenter: UsageContract.Presenter by lazy { AppManagerPresenter(context, this) }
    private val adapter: UsageStatAdapter by lazy { UsageStatAdapter(presenter, this) }
    private var descriptionTextView: TextView? = null

    private var timeSetting = INTERVAL_WEEK
    private var sortSetting = SORT_ALPHABETICALLY

    companion object {
        private const val TIME_KEY = "time_key"
        private const val SORT_KEY = "sort_key"
    }

    private var descriptionString: String? = null

    fun setDescription(description: String){
        descriptionString = description
        updateDescription()
    }

    private fun updateDescription(){
        if (descriptionString != null){
            descriptionTextView?.text = descriptionString
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (savedInstanceState != null) {
            timeSetting = savedInstanceState.getInt(TIME_KEY)
            sortSetting = savedInstanceState.getInt(SORT_KEY)
        }
        val rootView = inflater.inflate(R.layout.appmanager_fragment_layout, container, false)
        descriptionTextView = rootView.findViewById(R.id.feature_description) as TextView
        updateDescription()
        return rootView
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(TIME_KEY, timeSetting)
        outState.putInt(SORT_KEY, sortSetting)
        super.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        initRecyclerview()
        initToolbar()
        initSpinner()

        requestPermission.setOnClickListener { openSettings() }
        unistallAppsFAB.setOnClickListener { presenter.requestUnistall(activity) }
    }

    private fun initRecyclerview() {
        val manager = LinearLayoutManager(context)
        manager.isItemPrefetchEnabled = true
        recyclerview.layoutManager = manager
        recyclerview.adapter = adapter
    }

    private fun initToolbar() {
        toolbar.inflateMenu(R.menu.menu)
        toolbar.setOnMenuItemClickListener {
            openSortDialog()
            true
        }
    }

    private fun openSortDialog() {
        AlertDialog.Builder(ContextThemeWrapper(context, R.style.Theme_AppCompat_Light_Dialog))
                .setTitle(R.string.sort_by)
                .setSingleChoiceItems(R.array.sort, sortSetting) { dialog, selection ->
                    sortSetting = selection
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.no, null)
                .setOnDismissListener { queryForNewData() }
                .create()
                .show()
    }

    private fun queryForNewData() {
        emptyView.hide()
        progressBar.show()
        presenter.retrieveUsageStats(timeSetting, sortSetting)
    }

    private fun initSpinner() {
        val list = resources.getStringArray(R.array.time_range)
        val adapter = ArrayAdapter<String>(ContextThemeWrapper(context, R.style.ThemeOverlay_AppCompat_Light),
                android.R.layout.simple_spinner_item, list)
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_layout)

        time_spinner.adapter = adapter
        time_spinner.setSelection(INTERVAL_WEEK)
        time_spinner.onItemSelectedListener = adapterListener
    }

    private fun openSettings() = startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))

    override fun onResume() {
        super.onResume()
        unistallAppsFAB.hide()
        queryForNewData()
    }

    override fun onUsageStatsRetrieved(list: List<UsageStatsWrapper>) {
        progressBar.hide()
        permissionView.hide()
        updateViewStatusWith(list)
    }

    private fun updateViewStatusWith(list: List<UsageStatsWrapper>) {
        if (list.isNotEmpty()) {
            adapter.setList(list)
        } else {
            emptyView.show()
        }
    }

    override fun onUserHasNoPermission() {
        progressBar.hide()
        permissionView.show()
    }

    override fun toggleFABVisibility(show: Boolean) {
        if (show) unistallAppsFAB.show() else unistallAppsFAB.hide()
    }

    override fun onPackageSelected(packageName: String) {
        activity.startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", packageName, null)))
    }

    private val adapterListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if (position in INTERVAL_WEEK..NO_FILTER) {
                timeSetting = position
                queryForNewData()
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {
        }
    }
}
