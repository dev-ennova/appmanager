package it.ennova.appmanager.internal

import android.app.usage.UsageStats
import android.graphics.drawable.Drawable

data class UsageStatsWrapper(val usageStats: UsageStats?,
                             val appIcon: Drawable,
                             val appName: String,
                             val packageName: String,
                             var cacheSize: Float = 0f,
                             var codeSize: Float = 0f,
                             var dataSize: Float = 0f,
                             var checked: Boolean = false) {

    fun getSize() = codeSize + dataSize + cacheSize
}


