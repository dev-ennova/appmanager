package it.ennova.appmanager.internal

import android.annotation.TargetApi
import android.app.AppOpsManager
import android.app.AppOpsManager.MODE_ALLOWED
import android.app.AppOpsManager.OPSTR_GET_USAGE_STATS
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_DELETE
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.pm.IPackageStatsObserver
import android.content.pm.PackageManager
import android.content.pm.PackageStats
import android.net.Uri
import android.os.Build
import android.os.Process.myUid
import rx.Emitter
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action1
import rx.schedulers.Schedulers
import timber.log.Timber
import java.util.*

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
internal class AppManagerPresenter(private val context: Context, private val view: UsageContract.View) : UsageContract.Presenter {

    private val usageStatsManager: UsageStatsManager = context.getSystemService("usagestats") as UsageStatsManager
    private val packageManager: PackageManager = context.packageManager
    private var appToUnistall: List<UsageStatsWrapper> = ArrayList()

    private val appInstalled = Observable.fromCallable { packageManager.getInstalledApplications(getFlags()) }
            .flatMap { Observable.from(it) }
            .map { it.packageName }
            .filter { packageManager.isUserApp(it) }
            .onErrorResumeNext { t -> Observable.empty() }
            .subscribeOn(Schedulers.computation())
            .toList()


    override fun retrieveUsageStats(time: Int, sort: Int) {
        if (!checkForPermission(context)) {
            view.onUserHasNoPermission()
            return
        }

        appInstalled.zipWith(getUsageStats(time), { name, stats -> buildUsageStatsWrapper(name, stats) })
                .flatMap { Observable.from(it) }
                .filter { it.usageStats == null }
                .flatMap { getPackageSizeObservable(it) }
                .toSortedList { first, second -> sortBy(sort, first, second) }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.onUsageStatsRetrieved(it) }, { Timber.e(it) })
    }

    private fun sortBy(sort: Int, first: UsageStatsWrapper, second: UsageStatsWrapper): Int {
        return if (sort == SORT_ALPHABETICALLY)
            sortAlphabetically(first, second)
        else
            sortByDimension(first, second)
    }

    private fun sortAlphabetically(first: UsageStatsWrapper, second: UsageStatsWrapper): Int {
        return first.appName.compareTo(second.appName)
    }

    private fun sortByDimension(first: UsageStatsWrapper, second: UsageStatsWrapper): Int {
        return second.getSize().compareTo(first.getSize())
    }

    private fun getUsageStats(time: Int): Observable<List<UsageStats>> {
        return Observable.fromCallable {
            usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST,
                    getStartTime(time), System.currentTimeMillis())
        }
                .flatMap { Observable.from(it) }
                .groupBy { it.packageName }
                .flatMap { it.toList() }
                .map { getMostRecentEntryFromList(it) }
                .filter { it != null }
                .map { it!! }
                .subscribeOn(Schedulers.computation())
                .toList()
    }

    private fun checkForPermission(context: Context): Boolean {
        val appOps = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
        val mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS, myUid(), context.packageName)
        return mode == MODE_ALLOWED
    }

    private fun buildUsageStatsWrapper(packageNames: List<String>, usageStatses: List<UsageStats>): List<UsageStatsWrapper> {
        val list = ArrayList<UsageStatsWrapper>()
        for (name in packageNames) {
            var added = false
            for (stat in usageStatses) {
                if (name == stat.packageName) {
                    added = true
                    list.add(fromUsageStat(stat))
                }
            }
            if (!added) {
                list.add(fromUsageStat(name))
            }
        }
        return list
    }

    private fun getMostRecentEntryFromList(usageStatsList: List<UsageStats>): UsageStats? {
        if (usageStatsList.isEmpty()) {
            return null
        }
        var selected = usageStatsList[0]
        for (stat in usageStatsList) {
            if (selected.lastTimeUsed < stat.lastTimeUsed) {
                selected = stat
            }
        }
        return selected
    }

    @Throws(IllegalArgumentException::class)
    private fun fromUsageStat(packageName: String): UsageStatsWrapper {
        try {
            val ai = packageManager.getApplicationInfo(packageName, 0)
            return UsageStatsWrapper(null, packageManager.getApplicationIcon(ai),
                    packageManager.getApplicationLabel(ai).toString(), packageName)

        } catch (e: PackageManager.NameNotFoundException) {
            throw IllegalArgumentException(e)
        }
    }

    @Throws(IllegalArgumentException::class)
    private fun fromUsageStat(usageStats: UsageStats): UsageStatsWrapper {
        try {
            val ai = packageManager.getApplicationInfo(usageStats.packageName, 0)
            return UsageStatsWrapper(usageStats, packageManager.getApplicationIcon(ai),
                    packageManager.getApplicationLabel(ai).toString(), usageStats.packageName)

        } catch (e: PackageManager.NameNotFoundException) {
            throw IllegalArgumentException(e)
        }
    }

    fun getPackageSizeObservable(usageStatsWrapper: UsageStatsWrapper): Observable<UsageStatsWrapper> {

        return Observable.fromEmitter<UsageStatsWrapper>(object : Action1<Emitter<UsageStatsWrapper>> {
            val getPackageSizeInfo = packageManager.javaClass.kotlin.java.getMethod(
                    "getPackageSizeInfo", String::class.java, IPackageStatsObserver::class.java)

            override fun call(emitter: Emitter<UsageStatsWrapper>) {
                getPackageSizeInfo.invoke(packageManager, usageStatsWrapper.packageName, object : IPackageStatsObserver.Stub() {
                    override fun onGetStatsCompleted(pStats: PackageStats, succeeded: Boolean) {
                        usageStatsWrapper.dataSize = pStats.dataSize.formatMb()
                        usageStatsWrapper.cacheSize = pStats.cacheSize.formatMb()
                        usageStatsWrapper.codeSize = pStats.codeSize.formatMb()
                        emitter.onNext(usageStatsWrapper)
                        emitter.onCompleted()
                    }
                })
            }
        }, Emitter.BackpressureMode.BUFFER)
    }

    private fun getStartTime(time: Int): Long {
        val cal = Calendar.getInstance()
        when (time) {
            INTERVAL_WEEK -> cal.add(Calendar.WEEK_OF_MONTH, -7)
            INTERVAL_MONTH -> cal.add(Calendar.MONTH, -1)
            INTERVAL_YEAR -> cal.add(Calendar.YEAR, -1)
            NO_FILTER -> cal.add(Calendar.YEAR, -2)
        }
        return cal.timeInMillis
    }

    override fun onSomethingChanged(selectedPackages: List<UsageStatsWrapper>?) {
        appToUnistall = selectedPackages!!
        if (appToUnistall.isEmpty())
            view.toggleFABVisibility(false)
        else
            view.toggleFABVisibility(true)
    }

    override fun requestUnistall(ctx: Context) {
        for (app in appToUnistall) {
            val uri = Uri.parse("package:" + app.packageName)
            val intent = Intent(ACTION_DELETE, uri)
            intent.flags = FLAG_ACTIVITY_NEW_TASK
            ctx.startActivity(intent)
        }
        appToUnistall = ArrayList()
        view.toggleFABVisibility(false)
    }
}

