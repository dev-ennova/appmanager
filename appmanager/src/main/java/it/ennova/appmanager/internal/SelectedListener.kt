package it.ennova.appmanager.internal

internal interface SelectedListener {
    fun onSomethingChanged(selectedPackages: List<UsageStatsWrapper>?)
}