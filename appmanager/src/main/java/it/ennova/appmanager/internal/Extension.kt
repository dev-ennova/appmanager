package it.ennova.appmanager.internal

import android.animation.Animator
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Build
import android.view.View

@Throws(IllegalArgumentException::class)
fun PackageManager.isSystemApp(packageName: String): Boolean {
    try {
        val info = getApplicationInfo(packageName, 0)
        if (info.flags and ApplicationInfo.FLAG_SYSTEM != 1) {
            return false
        }
    } catch (e: PackageManager.NameNotFoundException) {
        throw IllegalArgumentException(e)
    }

    return true
}

@Throws(IllegalArgumentException::class)
fun PackageManager.isUserApp(packageName: String): Boolean {
    return !isSystemApp(packageName)
}

fun userHasNougat() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N

fun getFlags() = if (userHasNougat()) {
    PackageManager.GET_META_DATA or PackageManager.GET_SHARED_LIBRARY_FILES or PackageManager.MATCH_UNINSTALLED_PACKAGES
} else {
    PackageManager.GET_META_DATA or
            PackageManager.GET_SHARED_LIBRARY_FILES or
            PackageManager.GET_UNINSTALLED_PACKAGES
}

const val MB = 1024 * 1024

fun Long.formatMb(): Float {
    return this.toFloat()
            .div(MB)
}

fun Float.formatMb(digits: Int = 2) = String.format("%.${digits}f", this) + " MB"

fun View.hide() {
    animate().setDuration(200).alpha(0f).setListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {

        }

        override fun onAnimationEnd(animation: Animator?) {
            visibility = View.GONE
            alpha = 1f
        }

        override fun onAnimationCancel(animation: Animator?) {

        }

        override fun onAnimationStart(animation: Animator?) {
        }
    })

}

fun View.show() {
    animate().setDuration(200).alpha(1f).setListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {

        }

        override fun onAnimationEnd(animation: Animator?) {
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
            visibility = View.VISIBLE
            alpha = 0f
        }
    })

}
