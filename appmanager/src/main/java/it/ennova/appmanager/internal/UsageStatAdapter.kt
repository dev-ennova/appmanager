package it.ennova.appmanager.internal

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import it.ennova.appmanager.R
import java.util.*

internal class UsageStatAdapter(val listener: SelectedListener, val packageListener: PackageListener) :
        RecyclerView.Adapter<UsageStatVH>(), SelectedListener {

    private var list: List<UsageStatsWrapper> = ArrayList<UsageStatsWrapper>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsageStatVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.usage_stat_item, parent, false)
        return UsageStatVH(view, this, packageListener)
    }

    override fun onBindViewHolder(holder: UsageStatVH, position: Int) {
        holder.bindTo(list[position])
    }

    override fun getItemCount() = list.size

    fun setList(newList: List<UsageStatsWrapper>) {
        val diff = DiffUtil.calculateDiff(UsageStatsWrapperCallback(list, newList))
        this.list = newList
        diff.dispatchUpdatesTo(this)
    }

    override fun onSomethingChanged(selectedPackages: List<UsageStatsWrapper>?) {
        listener.onSomethingChanged(list.filter { it.checked })
    }
}
