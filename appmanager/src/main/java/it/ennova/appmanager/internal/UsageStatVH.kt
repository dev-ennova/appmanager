package it.ennova.appmanager.internal

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.usage_stat_item.view.*

internal class UsageStatVH(itemView: View, val listener: SelectedListener, val packageListener: PackageListener)
    : RecyclerView.ViewHolder(itemView) {

    fun bindTo(usageStatsWrapper: UsageStatsWrapper) {

        with(usageStatsWrapper) {

            itemView.setOnClickListener { packageListener.onPackageSelected(usageStatsWrapper.packageName) }

            itemView.icon.setImageDrawable(appIcon)
            itemView.title.text = appName

            itemView.selected.isChecked = checked
            itemView.selected.setOnClickListener {
                checked = !checked
                listener.onSomethingChanged(null)
            }

            itemView.space_occupied.text = getSize().formatMb()
        }
    }
}
