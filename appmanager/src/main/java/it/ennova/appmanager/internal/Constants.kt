package it.ennova.appmanager.internal

internal const val SORT_ALPHABETICALLY = 0
internal const val SORT_DIMENSION = 1

internal const val INTERVAL_WEEK = 0
internal const val INTERVAL_MONTH = 1
internal const val INTERVAL_YEAR = 2
internal const val NO_FILTER = 3
