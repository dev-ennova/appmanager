package it.ennova.appmanager.internal

internal interface PackageListener{
    fun onPackageSelected(packageName: String)
}
