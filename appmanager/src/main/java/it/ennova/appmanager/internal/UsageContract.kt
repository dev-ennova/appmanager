package it.ennova.appmanager.internal

import android.content.Context

internal interface UsageContract {

    interface View {
        fun onUsageStatsRetrieved(list: List<UsageStatsWrapper>)
        fun onUserHasNoPermission()
        fun toggleFABVisibility(show: Boolean)
    }

    interface Presenter : SelectedListener {
        fun retrieveUsageStats(time: Int, sort: Int)
        fun requestUnistall(ctx: Context)
    }
}
