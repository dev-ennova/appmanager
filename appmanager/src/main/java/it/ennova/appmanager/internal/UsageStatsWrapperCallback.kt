package it.ennova.appmanager.internal

import android.support.v7.util.DiffUtil

class UsageStatsWrapperCallback(val oldList: List<UsageStatsWrapper>?,
                                val newList: List<UsageStatsWrapper>?) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList!![oldItemPosition].packageName == newList!![newItemPosition].packageName
    }

    override fun getOldListSize() = oldList?.size ?: 0

    override fun getNewListSize() = newList?.size ?: 0

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList!![oldItemPosition] == newList!![newItemPosition]
    }
}
